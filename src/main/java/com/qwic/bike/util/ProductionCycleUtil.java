package com.qwic.bike.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.qwic.bike.domain.ProductionCycle;
import com.qwic.bike.exception.ApplicationException;
import com.qwic.bike.exception.ApplicationMessageKey;

public class ProductionCycleUtil {
	
	/**
	 * Method to format a date
	 * @param date
	 * @return - date formatted in "dd-MM-yyyy" pattern
	 */
	public static String formatDate(Date date) {
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		return simpleDateFormat.format(date);
	}
	
	/**
	 * Method to format the log to show the overlap between two dates
	 * @param productionsCycle1 - first production cycle planned
	 * @param productionsCycle2 - second production cycle planned
	 * @return - string with the formatted log to show overlap between two dates
	 */
	public static String formatOverLapLog(ProductionCycle productionsCycle1, ProductionCycle productionsCycle2) {
		String startDate1 = formatDate(productionsCycle1.getStartingDay());
		String endDate1 = formatDate(productionsCycle1.getEndingDay());

		String startDate2 = formatDate(productionsCycle2.getStartingDay());
		String endDate2 = formatDate(productionsCycle2.getEndingDay());
		
		StringBuilder strFormatted = new StringBuilder();
		strFormatted.append(startDate1).append(endDate1).append(" - ").append(startDate2).append(endDate2);
		
		return strFormatted.toString();
	}
	
	/**
	 * Method to set the ending date
	 * @param date - starting date
	 * @param days - duration of the cycle
	 * @return - end date
	 */
	public static Date getEndDate(Date date, int days) {
		if (days <= 0 || days >= 1000) {
			throw new ApplicationException(ApplicationMessageKey.INVALID_DURATION);
		}
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, days-1);

		return cal.getTime();
	}
	
	/**
	 * Method to get date without time in order to compare if the starting date and current date
	 * @param date
	 * @return - date without time
	 */
	public static Date getDateWithoutTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		date = calendar.getTime();
		return date;
	}
}
