package com.qwic.bike.service;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.qwic.bike.domain.ProductionCycle;
import com.qwic.bike.dto.ProductionCycleDto;
import com.qwic.bike.exception.ApplicationException;
import com.qwic.bike.exception.ApplicationMessageKey;
import com.qwic.bike.util.ProductionCycleUtil;

@Component
public class ProductionCycleServiceImpl implements ProductionCycleService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductionCycleServiceImpl.class);
	private static final int INVALID_QUANTITY = 100000;

	@Override
	public ProductionCycleDto calculateNumberOfProductionCycles(List<ProductionCycle> productionsCycle) {
		//Set the endDate to each production cycle planned
		productionsCycle.forEach(p -> p.setEndingDay(ProductionCycleUtil.getEndDate(p.getStartingDay(), p.getDuration())));
		
		//Sort the production cycle list based on starting date and ending date
		productionsCycle.sort(Comparator.comparing(ProductionCycle::getStartingDay).thenComparing(ProductionCycle::getEndingDay));
		
		Date currentDate = ProductionCycleUtil.getDateWithoutTime(new Date());

		int numberOfProductionCycles = productionsCycle.size();
		for (int i = 0; i < productionsCycle.size(); i++) {
			if (i+1 < productionsCycle.size()) {
				Date startDate = ProductionCycleUtil.getDateWithoutTime(productionsCycle.get(i).getStartingDay());
				if (startDate.before(currentDate)) {
					throw new ApplicationException(ApplicationMessageKey.INVALID_STARTING_DATE);
				}
				if (isOverLap(productionsCycle.get(i), productionsCycle.get(i+1))) {
					LOGGER.info("OverLap between: " + ProductionCycleUtil.formatOverLapLog(productionsCycle.get(i), productionsCycle.get(i+1)));
					//Decrease the number of cycle according to the overlap found
					numberOfProductionCycles--;
					//jump to the next cycle planned
					i++;
					
				}
			}
		}

		ProductionCycleDto productionCycleDto = new ProductionCycleDto(numberOfProductionCycles);

		if (productionCycleDto.getProductionCycle() > INVALID_QUANTITY) {
			throw new ApplicationException(ApplicationMessageKey.INVALID_QUANTITY);
		}

		return productionCycleDto;
	}

	/**
	 * Method to verify if there is an overlap between two dates 
	 * @param productionsCycle1 - first production cycle planned
	 * @param productionsCycle2 - second production cycle planned
	 * @return - if there is an overlap between two dates
	 */
	private boolean isOverLap(ProductionCycle productionsCycle1, ProductionCycle productionsCycle2) {
		Date startDate1 = productionsCycle1.getStartingDay();
		Date endDate1 = productionsCycle1.getEndingDay();

		Date startDate2 = productionsCycle2.getStartingDay();
		Date endDate2 = productionsCycle2.getEndingDay();

		return startDate1.before(endDate2) && endDate1.after(startDate2)
				|| endDate1.equals(startDate2);	
	}
}
