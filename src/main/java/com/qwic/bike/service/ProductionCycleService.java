package com.qwic.bike.service;


import java.util.List;

import com.qwic.bike.domain.ProductionCycle;
import com.qwic.bike.dto.ProductionCycleDto;

public interface ProductionCycleService {

	/**
	 * Method to calculate the number of production cycles
	 * @param productionsCycle - contains a list with production cycle planned
	 * @return productionCycleDto - number of production cycle based on the schedule
	 */
	ProductionCycleDto calculateNumberOfProductionCycles(List<ProductionCycle> productionsCycle);
}
