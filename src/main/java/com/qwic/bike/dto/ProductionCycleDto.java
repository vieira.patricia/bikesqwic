package com.qwic.bike.dto;

public class ProductionCycleDto {
	
	private int productionCycle;
	
	public ProductionCycleDto() {}

	public ProductionCycleDto(int productionCycle) {
		this.productionCycle = productionCycle;
	}

	public int getProductionCycle() {
		return productionCycle;
	}

	public void setProductionCycle(int productionCycle) {
		this.productionCycle = productionCycle;
	}
}
