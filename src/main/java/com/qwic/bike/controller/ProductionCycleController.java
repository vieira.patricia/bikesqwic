package com.qwic.bike.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.qwic.bike.domain.ProductionCycle;
import com.qwic.bike.dto.ProductionCycleDto;
import com.qwic.bike.service.ProductionCycleService;

@RestController
public class ProductionCycleController {
	
	private ProductionCycleService productionCycleService;

	public ProductionCycleController(ProductionCycleService productionCycleService) {
		this.productionCycleService = productionCycleService;
	}

	@RequestMapping(value = "/bikes/productionCycle", method = RequestMethod.POST)
	@ResponseBody
	public ProductionCycleDto generateNumberOfProductionCycle(
			@RequestBody List<ProductionCycle> productionCycleList) {   
		return productionCycleService.calculateNumberOfProductionCycles(productionCycleList);
	}
}
