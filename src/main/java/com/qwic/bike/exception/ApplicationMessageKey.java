package com.qwic.bike.exception;

import org.springframework.http.HttpStatus;

/**
 * Enum with general application error
 */
public enum ApplicationMessageKey {

	INVALID_STARTING_DATE("Starting date must be higher than current date", HttpStatus.BAD_REQUEST),
	INVALID_QUANTITY("Quantity must be less than 100000", HttpStatus.BAD_REQUEST),
	INVALID_DURATION("Duration must be higher than 0 and less than 1000", HttpStatus.BAD_REQUEST);
	
	String messageDescription;
	HttpStatus httpStatus;

	ApplicationMessageKey(String messageDescription, HttpStatus httpCode) {
		this.messageDescription = messageDescription;
		this.httpStatus = httpCode;
	}

	ApplicationMessageKey(String messageDescription) {
		this(messageDescription, HttpStatus.BAD_REQUEST);
	}  

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public String getMessageDescription() {
		return messageDescription;
	}

}
