package com.qwic.bike.domain;

import java.util.Date;

public class ProductionCycle {

	private Date startingDay;
	private Date endingDay;
	private int duration;
	
	public Date getStartingDay() {
		return startingDay;
	}
	public void setStartingDay(Date startingDay) {
		this.startingDay = startingDay;
	}
	public Date getEndingDay() {
		return endingDay;
	}
	public void setEndingDay(Date endingDay) {
		this.endingDay = endingDay;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
}
