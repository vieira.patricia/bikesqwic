package com.qwic.bike.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.qwic.bike.domain.ProductionCycle;
import com.qwic.bike.dto.ProductionCycleDto;
import com.qwic.bike.exception.ApplicationException;
import com.qwic.bike.exception.ApplicationMessageKey;



@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductionCycleServiceTest {

	private ProductionCycle productionCycle;
	private List<ProductionCycle> productionCycleList;
	private Calendar currentDate;
	private ProductionCycleService productionCycleService;
	private ProductionCycleDto productionCycleDto;

	@Rule
	public ExpectedException expectedException = ExpectedException.none();  


	@Before
	public void setUp() {
		productionCycleList = new ArrayList<>();
		productionCycleDto = new ProductionCycleDto();
		productionCycleService = new ProductionCycleServiceImpl();
		
		currentDate = Calendar.getInstance();
		
		createProductionCycleList();
	}

	@Test
	public void calculateNumberOfProductionCyclesTest() {
		productionCycleDto = productionCycleService.calculateNumberOfProductionCycles(productionCycleList);
		assertThat(productionCycleDto.getProductionCycle(),is(3));	
	}

	@Test
	public void calculateNumberOfProductionCyclesTobeOnlyOneTest() {
		//Created only one cycle		
		productionCycleList = new ArrayList<>();
		currentDate = Calendar.getInstance();
		productionCycleList.add(createProductionCycle(5, currentDate.getTime()));

		productionCycleDto = productionCycleService.calculateNumberOfProductionCycles(productionCycleList);
		assertThat(productionCycleDto.getProductionCycle(),is(1));	
	}

	@Test
	public void calculateNumberOfProductionCyclesWithEndDate1EqualsStartDate2Test() {
		currentDate = Calendar.getInstance();
		
		productionCycleList = new ArrayList<>();
		productionCycleList.add(createProductionCycle(5, currentDate.getTime()));
		
		//(Current Date + 4 days) + 5 days of duration - to simulate the enddate1 equals to startDate2
		currentDate.add(Calendar.DATE, 4);
		productionCycleList.add(createProductionCycle(5, currentDate.getTime()));
		
		currentDate.add(Calendar.MONTH, 5);
		productionCycleList.add(createProductionCycle(5, currentDate.getTime()));

		productionCycleDto = productionCycleService.calculateNumberOfProductionCycles(productionCycleList);
		assertThat(productionCycleDto.getProductionCycle(),is(2));	
	}

	@Test
	public void calculateNumberOfProductionCyclesTobeOnlyTwoTest() {
		currentDate = Calendar.getInstance();
		productionCycleList = new ArrayList<>();
		productionCycleList.add(createProductionCycle(5, currentDate.getTime()));

		//Current Date + 5 days days of duration to simulate an overlap
		productionCycleList.add(createProductionCycle(4, currentDate.getTime()));

		//(Current Date + 5 days) + 5 days of duration
		currentDate.add(Calendar.DATE, 5);
		productionCycleList.add(createProductionCycle(5, currentDate.getTime()));

		productionCycleDto = productionCycleService.calculateNumberOfProductionCycles(productionCycleList);
		assertThat(productionCycleDto.getProductionCycle(),is(2));	
	}

	@Test
	public void calculateNumberOfProductionCyclesThrowInvalidStartingDate() {
		expectedException.expect(ApplicationException.class);    
		expectedException.expectMessage(ApplicationMessageKey.INVALID_STARTING_DATE.getMessageDescription());
		List<ProductionCycle> productionCycleWithInvalidDate = createProductionCycleListWithInvalidDate();
		productionCycleDto = productionCycleService.calculateNumberOfProductionCycles(productionCycleWithInvalidDate);
	}

	@Test
	public void calculateNumberOfProductionCyclesThrowInvalidDuration() {
		expectedException.expect(ApplicationException.class);    
		expectedException.expectMessage(ApplicationMessageKey.INVALID_DURATION.getMessageDescription());
		List<ProductionCycle> productionCycleWithInvalidDate = createProductionCycleList();
		productionCycleWithInvalidDate.get(0).setDuration(1000);
		productionCycleDto = productionCycleService.calculateNumberOfProductionCycles(productionCycleWithInvalidDate);
	}

	/**
	 * Created a method to return a production cycle list with no overlap
	 * @return productioncycle list
	 */
	private List<ProductionCycle> createProductionCycleList() {
		currentDate = Calendar.getInstance();
		productionCycleList.add(createProductionCycle(5, currentDate.getTime()));

		currentDate.add(Calendar.MONTH, 1);
		productionCycleList.add(createProductionCycle(4, currentDate.getTime()));
		
		currentDate.add(Calendar.YEAR, 1);
		productionCycleList.add(createProductionCycle(4, currentDate.getTime()));
		
		return productionCycleList;
	}

	/**
	 * Created method to return a productionCycle list with an invalid date
	 * @return list of production cycle list
	 */
	private List<ProductionCycle> createProductionCycleListWithInvalidDate() {
		List<ProductionCycle> productionCycleList = new ArrayList<>();
		
		currentDate = Calendar.getInstance();
		//Starting date < Current date 
		currentDate.add(Calendar.YEAR, -1);
		productionCycleList.add(createProductionCycle(5, currentDate.getTime()));


		currentDate = Calendar.getInstance();
		productionCycleList.add(createProductionCycle(5, currentDate.getTime()));

		return productionCycleList;
	}
	
	
	private ProductionCycle createProductionCycle(int duration, Date date) {
		productionCycle = new ProductionCycle();
		productionCycle.setStartingDay(date);
		productionCycle.setDuration(5);
		
		return productionCycle;
	}
}
